﻿#include "Color.h"

const Color Color::Black(0u, 0u, 0u);
const Color Color::White(255u, 255u, 255u);
const Color Color::Red(255u, 0u, 0u);
const Color Color::Green(0u, 255u, 0u);
const Color Color::Blue(0u, 0u, 255u);
const Color Color::Yellow(255u, 255u, 0u);
const Color Color::Magenta(255u, 0u, 255u);
const Color Color::Cyan(0u, 255u, 255u);
const Color Color::Transparent(0u, 0u, 0u, 0u);

Color::Color()
	:r(0), g(0), b(0), a(255)
{
}

Color::Color(unsigned r, unsigned g, unsigned b)
	: r(r), g(g), b(b), a(255)
{
}

Color::Color(unsigned r, unsigned g, unsigned b, unsigned a)
	: r(r), g(g), b(b), a(a)
{
}
#pragma warning(push)
#pragma warning(disable: 4244)
Color::Color(float r, float g, float b, float a)
	: r(r*255), g(g*255), b(b*255), a(a*255)
{
}

Color::Color(float r, float g, float b)
	: r(r * 255), g(g * 255), b(b * 255), a(255)
{
}
#pragma warning(pop)

D3DCOLOR Color::AsD3DColor() const
{
	return D3DCOLOR_ARGB(a, r, g, b);
}

bool Color::operator==(const Color& right) const
{
	return this->a == right.a && this->b == right.b
		&& this->g == right.g && this->r == right.r;
}

bool Color::operator!=(const Color& right) const
{
	return !(*this == right);
}
