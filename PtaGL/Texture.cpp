﻿#include "Texture.h"
#include "GraphicsDevice.h"

Texture::Texture(GraphicsDevice *device, std::string filePath)
{
	if (!SUCCEEDED(D3DXCreateTextureFromFileEx(device->device, filePath.c_str(), D3DX_DEFAULT_NONPOW2, 
		D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 
		0, nullptr, nullptr, &texture)))
	{
		auto msg = "Failed to load texture: " + filePath;
		MessageBox(nullptr, msg.c_str(), nullptr, 0);
	}
	texture->GetLevelDesc(0, &desc);
}

Texture::Texture(): texture(nullptr)
{
	
}

Texture::~Texture()
{
	Release();
}

void Texture::Release()
{
	if (texture)
	{
		texture->Release();
		texture = nullptr;
	}
}

void Texture::Load(GraphicsDevice *device, std::string filePath)
{
	if (texture)
		texture->Release();

	if (!SUCCEEDED(D3DXCreateTextureFromFileEx(device->device, filePath.c_str(), D3DX_DEFAULT_NONPOW2,
		D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT,
		0, nullptr, nullptr, &texture)))
	{
		auto msg = "Failed to load texture: " + filePath;
		MessageBox(nullptr, msg.c_str(), nullptr, 0);
	}
	texture->GetLevelDesc(0, &desc);
}
