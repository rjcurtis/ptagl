﻿#pragma once
#include <d3d9.h>

class Color
{
public:
	Color();
	Color(unsigned int r, unsigned int g, unsigned int b);
	Color(unsigned int r, unsigned int g, unsigned int b, unsigned int a);
	Color(float r, float g, float b, float a);
	Color(float r, float g, float b);

	D3DCOLOR AsD3DColor() const;

	static const Color Black;       ///< Black predefined color
	static const Color White;       ///< White predefined color
	static const Color Red;         ///< Red predefined color
	static const Color Green;       ///< Green predefined color
	static const Color Blue;        ///< Blue predefined color
	static const Color Yellow;      ///< Yellow predefined color
	static const Color Magenta;     ///< Magenta predefined color
	static const Color Cyan;        ///< Cyan predefined color
	static const Color Transparent; ///< Transparent (black) predefined color

	unsigned int r, g, b, a;

	bool operator ==(const Color& right) const;
	bool operator !=(const Color& right) const;
};
