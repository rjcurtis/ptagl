#include "GraphicsDevice.h"

GraphicsDevice::GraphicsDevice()
{}
GraphicsDevice::~GraphicsDevice()
{
	if (device)
	{
		device->Release();
		device = nullptr;
	}

	if (direct3d)
	{
		direct3d->Release();
		direct3d = nullptr;
	}
}

bool GraphicsDevice::Initialize(HWND hWnd, bool windowed)
{
	direct3d = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS presentationParams;
	ZeroMemory(&presentationParams, sizeof(D3DPRESENT_PARAMETERS));
	presentationParams.Windowed = windowed;
	presentationParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
	presentationParams.hDeviceWindow = hWnd;

	if(!SUCCEEDED(direct3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_MIXED_VERTEXPROCESSING, &presentationParams, &device)))
	{
		return false;
	}

	return true;
}

void GraphicsDevice::Clear(D3DCOLOR color) const
{
	device->Clear(0, nullptr, D3DCLEAR_TARGET, color, 1.0f, 0);
}

void GraphicsDevice::Begin() const
{
	device->BeginScene();
}

void GraphicsDevice::End() const
{
	device->EndScene();
}

void GraphicsDevice::Present() const
{
	device->Present(nullptr, nullptr, nullptr, nullptr);
}
