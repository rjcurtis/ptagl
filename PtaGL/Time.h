﻿#pragma once

#include <Windows.h>

class Time
{
public:
	Time();

	float elapsedSeconds;
	float total;

	bool Initialize();
	void Update();
private:

	LONGLONG start;
	float frequencySeconds;
};
