#pragma once

#include <d3dx9.h>
#include "Texture.h"
#include "GraphicsDevice.h"
#include "RenderRegion.h"
#include "Color.h"
#include "Transformable.h"

class Sprite : public Transformable
{
public:
	Sprite();
	Sprite(GraphicsDevice *device, Texture *texture);
	Sprite(GraphicsDevice *device, Texture *texture, RenderRegion region);
	virtual ~Sprite();

	void SetTexture(GraphicsDevice *device, Texture *texture);

	virtual void Update(float gameTime);
	virtual void Draw(float gameTime);

private:
	Texture *texture;
	LPD3DXSPRITE sprite;

	Color color;
	bool initialized;
	RenderRegion renderRegion;
};
