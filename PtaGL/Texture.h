﻿#pragma once

#include <string>
#include <d3dx9.h>

class GraphicsDevice;

class Texture
{
public:
	explicit Texture(GraphicsDevice *device, std::string filePath);
	Texture();
	~Texture();
	void Release();
	void Load(GraphicsDevice* device, std::string filePath);
	int GetWidth() const { return desc.Width; }
	int GetHeight() const { return desc.Height; }
	LPDIRECT3DTEXTURE9 texture;
	D3DSURFACE_DESC desc;

private:
};
