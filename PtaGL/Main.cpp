/*#include <Windows.h>
#include "Game.h"

bool GenerateWindow(HINSTANCE hInstance, int nCmdShow, LPCSTR className, LPCSTR windowTitle, int width, int height, HWND& hWnd);
bool GenerateWindow(HINSTANCE hInstance, int nCmdShow, LPCSTR className, LPCSTR windowTitle, int x, int y, int width, int height, HWND& hWnd);

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HWND hWnd;
	if (GenerateWindow(hInstance, nCmdShow, "WndClassName", "TestWindow", 800, 600, hWnd))
	{
		MSG msg;

		Game game;
		game.Initialize(hWnd);

		while (true)
		{
			while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);  // translates virtual messages to character messages
				DispatchMessage(&msg);  // Dispatches the message to the HWND we are hosting
			}

			if (msg.message == WM_QUIT) break;

			game.Run();			
		}
		return msg.wParam;
	}

	return 0;
}

bool GenerateWindow(HINSTANCE hInstance, int nCmdShow, LPCSTR className, LPCSTR windowTitle, int width, int height, HWND& hWnd)
{
	auto displayWidth = GetSystemMetrics(SM_CXSCREEN);
	auto displayHeight = GetSystemMetrics(SM_CYSCREEN);
	auto centerX = (displayWidth - width) / 2;
	auto centerY = (displayHeight - height) / 2;
	return GenerateWindow(hInstance, nCmdShow, className, windowTitle, centerX, centerY, width, height, hWnd);
}

bool GenerateWindow(HINSTANCE hInstance, int nCmdShow, LPCSTR className, LPCSTR windowTitle, int x, int y, int width, int height, HWND& hWnd)
{
	WNDCLASSEX wcex;

	ZeroMemory(&wcex, sizeof(WNDCLASSEX));
	wcex.cbSize = sizeof(wcex);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WindowProc;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW);
	wcex.lpszClassName = className;
	wcex.hIconSm = LoadIcon(nullptr, IDI_WINLOGO);

	if (!RegisterClassEx(&wcex))
	{
		return false;
	}

	auto style = WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX; // disable resizing and maximizing

	hWnd = CreateWindowEx(0, className, windowTitle, style, x, y, width, height, nullptr, nullptr, hInstance, nullptr);
	ShowWindow(hWnd, nCmdShow);
	return true;
}

/*
Handles window messages
#1#
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	// Let windows take care of redrawing the window for now
	return DefWindowProc(hWnd, message, wParam, lParam);
}*/