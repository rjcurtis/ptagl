﻿#pragma once

#include "GraphicsDevice.h"
#include "Sprite.h"
#include "Time.h"
#include "Font.h"

class Game
{
public:
	Game();
	~Game();

	bool Initialize(HWND hWnd);
	void Run();
	void Update(float gameTime);
	void Draw(float gameTime);

private:
	GraphicsDevice *device;
	Sprite *player;
	Sprite *player2;
	Time time;
	Font *font;

	Texture *texture;
};
