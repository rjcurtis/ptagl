#include "Sprite.h"

Sprite::Sprite()
	: Transformable(), texture(nullptr), sprite(nullptr), color(255u, 255u, 255u, 255u), initialized(false)
{
	position.x = 0;
	position.y = 0;
	position.z = 0;
}

Sprite::Sprite(GraphicsDevice *device, Texture *texture)
	:Sprite()
{
	this->SetTexture(device, texture);
	this->renderRegion.width = width;
	this->renderRegion.height = height;
	this->renderRegion.x = this->renderRegion.y = 0;
}

Sprite::Sprite(GraphicsDevice* device, Texture* texture, RenderRegion region)
	: Sprite()
{
	this->SetTexture(device, texture);
	this->renderRegion = region;
}

Sprite::~Sprite()
{
	if (sprite)
	{
		sprite->Release();
		sprite = nullptr;
	}
	if (texture)
	{
		texture->Release();
		texture = nullptr;
	}
}

void Sprite::SetTexture(GraphicsDevice *device, Texture *texture)
{
	this->texture = texture;

	if (!SUCCEEDED(D3DXCreateSprite(device->device, &sprite)))
	{
		MessageBox(nullptr, "Error creating Sprite", nullptr, 0);
	}

	width = texture->desc.Width;
	height = texture->desc.Height;
}

void Sprite::Update(float gameTime)
{
}

void Sprite::Draw(float gameTime)
{
	sprite->Begin(D3DXSPRITE_ALPHABLEND);

	D3DXVECTOR2 center { width * scale / 2.0f, height * scale / 2.0f };
	D3DXVECTOR2 translate{ position.x, position.y };
	D3DXVECTOR2 scaling{ scale, scale };
	D3DXMATRIX matrix;
	D3DXMatrixTransformation2D(&matrix, nullptr, 0.f, &scaling, &center, rotation, &translate);

	RECT rect;
	rect.top = renderRegion.y;
	rect.left = renderRegion.x;
	rect.bottom = rect.top + renderRegion.height;
	rect.right = rect.left + renderRegion.width;

	sprite->SetTransform(&matrix);
	sprite->Draw(texture->texture, &rect, nullptr, nullptr, color.AsD3DColor());
	sprite->End();
}
