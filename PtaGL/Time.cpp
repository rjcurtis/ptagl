﻿#include "Time.h"

Time::Time()
	:elapsedSeconds(0), total(0), start(0), frequencySeconds(0)
{
}

bool Time::Initialize()
{
	LARGE_INTEGER i;

	if (!QueryPerformanceFrequency(&i))
	{
		return false;
	}
	frequencySeconds = static_cast<float>(i.QuadPart);
	if (!QueryPerformanceCounter(&i))
	{
		return false;
	}
	start = i.QuadPart;
	return true;
}

void Time::Update()
{
	LARGE_INTEGER i;

	QueryPerformanceCounter(&i);
	elapsedSeconds = static_cast<float>(i.QuadPart - start) / frequencySeconds;
	start = i.QuadPart;
}
