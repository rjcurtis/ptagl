#pragma once

#include <d3d9.h>

class GraphicsDevice
{
public:
	GraphicsDevice();
	~GraphicsDevice();

	bool Initialize(HWND hWnd, bool windowed);
	void Clear(D3DCOLOR color) const;
	void Begin() const;
	void End() const;
	void Present() const;

	LPDIRECT3DDEVICE9 device;
private:
	LPDIRECT3D9 direct3d;
};