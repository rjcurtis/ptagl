﻿#include "Game.h"
#include <math.h>

float accum = 0;

Game::Game() : device(nullptr), player(nullptr), player2(nullptr), font(nullptr), texture(nullptr)
{
}

Game::~Game()
{
	if (player2)
	{
		delete player2;
		player2 = nullptr;
	}
	if (player)
	{
		delete player;
		player = nullptr;
	}
	if (device)
	{
		delete device;
		device = nullptr;
	}
	if (font)
	{
		delete font;
		font = nullptr;
	}
}

bool Game::Initialize(HWND hWnd)
{
	device = new GraphicsDevice();
	if (!device->Initialize(hWnd, true))
	{
		return false;
	}

	texture = new Texture { device, "Images\\player.png" };
	player = new Sprite{ device, texture };
	player->SetX(100); player->SetY(400);
	player2 = new Sprite{ device, texture };
	player2->SetX(800/2); player2->SetY(600/2);
	font = new Font(device, 50, "Arial");
	time.Initialize();

	return true;
}

void Game::Run()
{
	time.Update();

	Update(time.elapsedSeconds);
	Draw(time.elapsedSeconds);
}

void Game::Update(float gameTime)
{
}

void Game::Draw(float gameTime)
{
	accum += gameTime;
	device->Clear(D3DCOLOR_XRGB(0, 100, 100));
	device->Begin();
	player->Draw(gameTime);
	player2->SetRotation(player2->GetRotation() + 5 * gameTime);
	player2->SetScale(std::sin(accum));
	player2->Draw(gameTime);
	font->Draw("Hello!");
	device->End();
	device->Present();
}